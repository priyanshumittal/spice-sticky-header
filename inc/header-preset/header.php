<?php
/*
-------------------------------------------------------------------------------
 Header Preset
-------------------------------------------------------------------------------*/
if ( ! function_exists( 'ssh_header_preset' ) ) {

	function ssh_header_preset() {
	    include_once('left-logo.php');
	    if (get_theme_mod('search_effect_style', 'toggle') != 'toggle'): ?>
	        <div id="searchbar_fullscreen" <?php if (get_theme_mod('search_effect_style', 'popup_light') == 'popup_light'): ?> class="bg-light" <?php endif; ?>>
	            <button type="button" class="close">×</button>
	            <form method="get" id="searchform" autocomplete="off" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
	            	<label>
	            		<input autofocus type="search" class="search-field" placeholder="<?php echo esc_attr_x('Search', 'placeholder', 'spice-sticky-header' ); ?>" value="" name="s" id="s" autofocus>
	            	</label>
	            	<input type="submit" class="search-submit btn" value="<?php echo esc_html__('Search', 'spice-sticky-header'); ?>">
	            </form>
	        </div>
	    <?php
	    endif;
	}
	add_action('ssh_header','ssh_header_preset');

}