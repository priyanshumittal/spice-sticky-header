<?php
/*
-------------------------------------------------------------------------------
 Sticky Header
-------------------------------------------------------------------------------*/
if (!function_exists('ssh_plus_fn')) :

	function ssh_plus_fn() {
			include_once(SSH_PLUGIN_DIR.'inc/sticky-header/sticky-with' . get_theme_mod('ssh_animation_effect', '') . '-animation.php');?>
			<style type="text/css">
        		.header-sticky.stickymenu1, .header-sticky.stickymenu, .header-sticky.shrink1 {
        			opacity: <?php echo get_theme_mod('ssh_opacity', '1.0'); ?> ;
        		}
        		.header-sticky.stickymenu1, .header-sticky.stickymenu {
        			<?php if (get_theme_mod('ssh_height', 0) > 0): ?>;
		            padding-top: <?php echo get_theme_mod('ssh_height', 0); ?>px;
		            padding-bottom: <?php echo get_theme_mod('ssh_height', 0); ?>px;
        			<?php endif; ?>
        		}
        		.header-sticky.shrink1 {
        			padding-top: 0;
		            padding-bottom: 0;
        		}
        	</style>
		<?php
		if (get_theme_mod('ssh_logo_enable', false) == true):	
		$sticky_header_logo_id 	= get_theme_mod('custom_logo');
    	$sticky_header_logo_image 	= wp_get_attachment_image_src($sticky_header_logo_id, 'full');
    	if (get_theme_mod('ssh_device_type', 'desktop') == 'desktop')  {
    		$ssh_logo_enable_desktop = get_theme_mod('ssh_logo_enable_desktop', '');
    		if (!empty($ssh_logo_enable_desktop)) { ?>
    			<a href="<?php echo esc_url(home_url('/')); ?>" class="sticky-logo-link" style="display: none;">
                	<img src="<?php echo esc_url($ssh_logo_enable_desktop); ?>" class="custom-logo">
               	</a>
    		<?php }
    		else { ?>
    			<a href="<?php echo esc_url(home_url('/')); ?>" class="sticky-logo-link" style="display: none;">
                	<img src="<?php if(!empty($sticky_header_logo_id)) { echo esc_url($sticky_header_logo_image[0]); } ?>" class="custom-logo">
                </a>
    		<?php }
    	}
    	elseif (get_theme_mod('ssh_device_type', 'desktop') == 'mobile')  {
    		$ssh_logo_enable_mobile = get_theme_mod('ssh_logo_enable_mobile', '');
    		if (!empty($ssh_logo_enable_mobile)) { ?>
    			<a href="<?php echo esc_url(home_url('/')); ?>" class="sticky-logo-mb-link" style="display: none;">
                	<img src="<?php echo esc_url($ssh_logo_enable_mobile); ?>" class="custom-logo">
               	</a>
    		<?php }
    		else { ?>
    			<a href="<?php echo esc_url(home_url('/')); ?>" class="sticky-logo-mb-link" style="display: none;">
                	<img src="<?php if(!empty($sticky_header_logo_id)) { echo esc_url($sticky_header_logo_image[0]); } ?>" class="custom-logo">
                </a>
    		<?php }
    	}
    	else {
    		$ssh_logo_enable_desktop = get_theme_mod('ssh_logo_enable_desktop', '');
    		if (!empty($ssh_logo_enable_desktop)) { ?>
    			<a href="<?php echo esc_url(home_url('/')); ?>" class="sticky-logo-link" style="display: none;">
                	<img src="<?php echo esc_url($ssh_logo_enable_desktop); ?>" class="custom-logo">
               	</a>
    		<?php }
    		else { ?>
    			<a href="<?php echo esc_url(home_url('/')); ?>" class="sticky-logo-link" style="display: none;">
                	<img src="<?php if(!empty($sticky_header_logo_id)) { echo esc_url($sticky_header_logo_image[0]); } ?>" class="custom-logo">
                </a>
    		<?php }

    		$ssh_logo_enable_mobile = get_theme_mod('ssh_logo_enable_mobile', '');
    		if (!empty($ssh_logo_enable_mobile)) { ?>
    			<a href="<?php echo esc_url(home_url('/')); ?>" class="sticky-logo-mb-link" style="display: none;">
                	<img src="<?php echo esc_url($ssh_logo_enable_mobile); ?>" class="custom-logo">
               	</a>
    		<?php }
    		else { ?>
    			<a href="<?php echo esc_url(home_url('/')); ?>" class="sticky-logo-mb-link" style="display: none;">
                	<img src="<?php if(!empty($sticky_header_logo_id)) { echo esc_url($sticky_header_logo_image[0]); } ?>" class="custom-logo">
                </a>
    		<?php }
    	}

    	endif;


    	//Color Schema For Sticky Header
    	if(get_theme_mod('enable_sticky_header_clr',false) == true ):?>
    		<style>
 				body .header-sticky.stickymenu .custom-logo-link-url .site-title a, body .header-sticky.stickymenu1 .custom-logo-link-url .site-title a, body .header-sticky.shrink1 .custom-logo-link-url .site-title a
 				{
                	color: <?php echo esc_attr( get_theme_mod('sticky_site_title_link_color', '#fff') ); ?>;
            	}
            	body .header-sticky.stickymenu .custom-logo-link-url .site-description, body .header-sticky.stickymenu1 .custom-logo-link-url .site-description, body .header-sticky.shrink1 .custom-logo-link-url .site-description  
            	{
                	color: <?php echo esc_attr( get_theme_mod('sticky_site_tagline_link_color', '#c5c5c5') ); ?>;
            	}
            	body .header-sticky.stickymenu.spice-custom.trsprnt-menu, body .header-sticky.stickymenu.layout1.spice-custom, body .header-sticky.stickymenu.layout2.spice-custom, body .header-sticky.stickymenu1.spice-custom.trsprnt-menu, body .header-sticky.stickymenu1.layout1.spice-custom, body .header-sticky.stickymenu1.layout2.spice-custom, body .header-sticky.shrink1.spice-custom.trsprnt-menu, body .header-sticky.shrink1.layout1.spice-custom, body .header-sticky.shrink1.layout2.spice-custom  
            	{
                	background-color: <?php echo esc_attr( get_theme_mod('sticky_menu_back_color', 'rgba(0, 0, 0, 0.7)') ); ?>;
            	}
            	body .header-sticky.stickymenu .spice-nav > li.parent-menu a, body .header-sticky.stickymenu1 .spice-nav > li.parent-menu a, body .header-sticky.shrink1 .spice-nav > li.parent-menu a, body .stickymenu.spice-custom .cart-header > a.cart-icon, body .stickymenu.spice-custom .spice-nav li > a, body .stickymenu1.spice-custom .cart-header > a.cart-icon, body .stickymenu1.spice-custom .spice-nav li > a, body .shrink1.spice-custom .cart-header > a.cart-icon, body .shrink1.spice-custom .spice-nav li > a 
            	{
                	color: <?php echo esc_attr( get_theme_mod('sticky_menu_link_color', '#ffffff') ); ?>;
            	}
            	body .header-sticky.stickymenu .spice-nav > li.parent-menu a:hover, body .header-sticky.stickymenu .spice-nav > li.parent-menu.dropdown a:hover, body .header-sticky.stickymenu1 .spice-nav > li.parent-menu a:hover, body .header-sticky.stickymenu1 .spice-nav > li.parent-menu.dropdown a:hover, body .header-sticky.shrink1 .spice-nav > li.parent-menu a:hover, body .header-sticky.shrink1 .spice-nav > li.parent-menu.dropdown a:hover,      body .stickymenu.spice-custom .cart-header > a.cart-icon:hover, body .stickymenu.spice-custom .spice-nav li > a:hover, body .stickymenu1.spice-custom .cart-header > a.cart-icon:hover, body .stickymenu1.spice-custom .spice-nav li > a:hover, body .shrink1.spice-custom .cart-header > a.cart-icon:hover, body .shrink1.spice-custom .spice-nav li > a:hover
            	{
                	color: <?php echo esc_attr( get_theme_mod('sticky_menu_link_hover_color', '#ff6f61') ); ?>;
            	}
            	body .header-sticky.stickymenu.spice-custom .spice-nav > .active > a, body .header-sticky.stickymenu.spice-custom .spice-nav .open .dropdown-menu > .active > a, body .header-sticky.stickymenu1.spice-custom .spice-nav > .active > a, body .header-sticky.stickymenu1.spice-custom .spice-nav .open .dropdown-menu > .active > a, body .header-sticky.shrink1.spice-custom .spice-nav > .active > a, body .header-sticky.shrink1.spice-custom .spice-nav .open .dropdown-menu > .active > a
            	{
                	color: <?php echo esc_attr( get_theme_mod('sticky_menu_active_link_color', '#ff6f61') ); ?>;
            	}
            	body .header-sticky.stickymenu.spice-custom .dropdown-menu, body .header-sticky.stickymenu1.spice-custom .dropdown-menu, body .header-sticky.shrink1.spice-custom .dropdown-menu 
            	{
                	background-color: <?php echo esc_attr( get_theme_mod('sticky_submenu_back_color', '#21202e') ); ?>;
            	}
            	body .header-sticky.stickymenu.spice-custom .dropdown-menu > li > a, body .header-sticky.stickymenu1.spice-custom .dropdown-menu > li > a, body .header-sticky.shrink1.spice-custom .dropdown-menu > li > a 
            	{
                	color: <?php echo esc_attr( get_theme_mod('sticky_submenu_link_color', '#d5d5d5') ); ?>;
            	}
            	body .header-sticky.stickymenu.spice-custom .spice-nav .dropdown-menu > li > a:hover, body .header-sticky.stickymenu1.spice-custom .spice-nav .dropdown-menu > li > a:hover, body .header-sticky.shrink1.spice-custom .spice-nav .dropdown-menu > li > a:hover 
            	{
                	color: <?php echo esc_attr( get_theme_mod('sticky_submenu_link_hover_color', '#ffffff') ); ?>;
            	}
			</style>
    	<?php
    	endif;	
	}
	add_action('ssh_plus_feature','ssh_plus_fn');

endif;