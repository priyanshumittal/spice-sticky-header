<?php
if(get_theme_mod('ssh_device_type','desktop')=='desktop' && get_theme_mod('ssh_logo_enable',false)==true)
{ ?>
    <script>
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (window.matchMedia("(max-width: 767px)").matches)  
                { 
                    //console.log("This is a mobile device."); 
                    jQuery('.spice.spice-custom').removeClass('stickymenu1');
                    jQuery('.custom-logo-link').show();
                    jQuery(".sticky-logo-link").hide(); 
                } 
                else { 
                    // The viewport is at least 768 pixels wide 
                    if (jQuery(window).scrollTop() > 200) 
                    {
                          jQuery('.header-sticky').addClass('stickymenu1');
                          jQuery(".sticky-logo-link").show();
                          jQuery(".custom-logo-link").hide();
                          jQuery('.header-sticky').slideDown();
                    } else {
                          jQuery('.header-sticky').removeClass('stickymenu1');
                          jQuery(".sticky-logo-link").hide();
                          jQuery(".custom-logo-link").show();
                          jQuery('.header-sticky').attr('style','');
                    }
                } 
            });
        });
    </script>
<?php 
}
elseif(get_theme_mod('ssh_device_type','desktop')=='mobile' && get_theme_mod('ssh_logo_enable',false)==true)
{ ?>
    <script>
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (window.matchMedia("(max-width: 767px)").matches)  
                { 
                    //this script call only mobile device
                    if (jQuery(window).scrollTop() > 200) {
                        jQuery('.header-sticky').addClass('stickymenu1');
                        jQuery(".sticky-logo-mb-link").show();
                        jQuery(".custom-logo-link").hide();
                        jQuery('.header-sticky').slideDown();
                    } else {
                        jQuery('.header-sticky').removeClass('stickymenu1');
                        jQuery(".sticky-logo-mb-link").hide();
                        jQuery(".custom-logo-link").show();
                        jQuery('.header-sticky').attr('style','');
                    }
                } 
                else { 
                    // This script call only for Desktop Device 
                    jQuery('.spice.spice-custom').removeClass('stickymenu1'); 
                    jQuery(".sticky-logo-mb-link").hide();
                    jQuery(".custom-logo-link").show();
                } 
            });
        });
    </script>
<?php  
}
elseif(get_theme_mod('ssh_device_type','desktop')=='both' && get_theme_mod('ssh_logo_enable',false)==true)
{ ?>
    <script>
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (window.matchMedia("(max-width: 767px)").matches)  
                { 
                    //this script call only mobile device
                    if (jQuery(window).scrollTop() > 200) {
                        jQuery('.header-sticky').addClass('stickymenu1');
                        jQuery(".sticky-logo-mb-link").show();
                        jQuery(".sticky-logo-link").hide();
                        jQuery(".custom-logo-link").hide();
                        jQuery('.header-sticky').slideDown();
                    } 
                    else
                    {
                        jQuery('.header-sticky').removeClass('stickymenu1');
                        jQuery(".sticky-logo-mb-link").hide();
                        jQuery(".sticky-logo-link").hide();
                        jQuery(".custom-logo-link").show();
                        jQuery('.header-sticky').attr('style','');
                    }
                } 
                else { 
                    // This script call only for Desktop Device 
                    if (jQuery(window).scrollTop() > 200) {
                        jQuery('.header-sticky').addClass('stickymenu1');
                        jQuery(".sticky-logo-mb-link").hide();
                        jQuery(".sticky-logo-link").show();
                        jQuery(".custom-logo-link").hide();
                        jQuery('.header-sticky').slideDown();
                    } 
                    else
                    {
                        jQuery('.header-sticky').removeClass('stickymenu1');
                        jQuery(".sticky-logo-mb-link").hide();
                        jQuery(".sticky-logo-link").hide();
                        jQuery(".custom-logo-link").show();
                        jQuery('.header-sticky').attr('style','');
                    }
                } 
            });
        });
    </script>
<?php  
}
elseif(get_theme_mod('ssh_device_type','desktop')=='desktop' && get_theme_mod('ssh_logo_enable',false)==false)
{ ?>
    <script type="text/javascript">
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (window.matchMedia("(max-width: 767px)").matches)  
                { 
                    //console.log("This is a mobile device."); 
                    jQuery('.spice.spice-custom').removeClass('stickymenu1');
                } 
                else { 
                    // The viewport is at least 768 pixels wide 
                    if(jQuery(window).scrollTop() > 200) {
                        jQuery('.header-sticky').addClass('stickymenu1');
                        jQuery('.header-sticky').slideDown();
                    } 
                    else {
                        jQuery('.header-sticky').removeClass('stickymenu1');
                        jQuery('.header-sticky').attr('style','');
                    }
                } 
            });
        });  
    </script>
 <?php 
}
elseif(get_theme_mod('ssh_device_type','desktop')=='mobile' && get_theme_mod('ssh_logo_enable',false)==false)
{ ?>
    <script>
    jQuery(document).ready(function ( jQuery ) {
        jQuery(window).bind('scroll', function () {
            if (window.matchMedia("(max-width: 767px)").matches)  
            { 
                //this script call only mobile device
                if (jQuery(window).scrollTop() > 200) {
                    jQuery('.header-sticky').addClass('stickymenu1');
                    jQuery('.header-sticky').slideDown();
                } 
                else {
                    jQuery('.header-sticky').removeClass('stickymenu1');
                    jQuery('.header-sticky').attr('style','');
                }
            } 
            else { 
                // This script call only for Desktop Device 
                jQuery('.spice.spice-custom').removeClass('stickymenu1');
            } 
        });
    });
    </script>
<?php
}
elseif(get_theme_mod('ssh_device_type','desktop')=='both' && get_theme_mod('ssh_logo_enable',false)==false)
{?>
    <script>
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (jQuery(window).scrollTop() > 200) {
                    jQuery('.header-sticky').addClass('stickymenu1');
                    jQuery('.header-sticky').slideDown();
                } 
                else {
                    jQuery('.header-sticky').removeClass('stickymenu1');
                    jQuery('.header-sticky').attr('style','');
                }
            });
        });
    </script>
<?php } ?>