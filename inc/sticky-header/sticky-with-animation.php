<?php
if(get_theme_mod('ssh_device_type','desktop')=='desktop' && get_theme_mod('ssh_logo_enable',false)==true)
{ ?>
    <script>
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (window.matchMedia("(max-width: 767px)").matches)  
                { 
                    //console.log("This is a mobile device."); 
                    jQuery('.spice.spice-custom').removeClass('stickymenu');
                    jQuery('.custom-logo-link').show();
                    jQuery(".sticky-logo-link").hide(); 
                } 
                else { 
                    // The viewport is at least 768 pixels wide 
                    if (jQuery(window).scrollTop() > 100) 
                    {
                          jQuery('.header-sticky').addClass('stickymenu');
                          jQuery(".sticky-logo-link").show();
                          jQuery(".custom-logo-link").hide();
                    } else {
                          jQuery('.header-sticky').removeClass('stickymenu');
                          jQuery(".sticky-logo-link").hide();
                          jQuery(".custom-logo-link").show();
                    }
                } 
            });
        });
    </script>
<?php 
}
elseif(get_theme_mod('ssh_device_type','desktop')=='mobile' && get_theme_mod('ssh_logo_enable',false)==true)
{ ?>
    <script>
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (window.matchMedia("(max-width: 767px)").matches)  
                { 
                    //this script call only mobile device
                    if (jQuery(window).scrollTop() > 100) {
                        jQuery('.header-sticky').addClass('stickymenu');
                        jQuery(".sticky-logo-mb-link").show();
                        jQuery(".custom-logo-link").hide();
                    } else {
                        jQuery('.header-sticky').removeClass('stickymenu');
                        jQuery(".sticky-logo-mb-link").hide();
                        jQuery(".custom-logo-link").show();
                    }
                } 
                else { 
                    // This script call only for Desktop Device 
                    jQuery('.spice.spice-custom').removeClass('stickymenu'); 
                    jQuery(".sticky-logo-mb-link").hide();
                    jQuery(".custom-logo-link").show();
                } 
            });
        });
    </script>
<?php  
}
elseif(get_theme_mod('ssh_device_type','desktop')=='both' && get_theme_mod('ssh_logo_enable',false)==true)
{ ?>
    <script>
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (window.matchMedia("(max-width: 767px)").matches)  
                { 
                    //this script call only mobile device
                    if (jQuery(window).scrollTop() > 100) {
                        jQuery('.header-sticky').addClass('stickymenu');
                        jQuery(".sticky-logo-mb-link").show();
                        jQuery(".sticky-logo-link").hide();
                        jQuery(".custom-logo-link").hide();
                    } 
                    else
                    {
                        jQuery('.header-sticky').removeClass('stickymenu');
                        jQuery(".sticky-logo-mb-link").hide();
                        jQuery(".sticky-logo-link").hide();
                        jQuery(".custom-logo-link").show();
                    }
                } 
                else { 
                    // This script call only for Desktop Device 
                    if (jQuery(window).scrollTop() > 100) {
                        jQuery('.header-sticky').addClass('stickymenu');
                        jQuery(".sticky-logo-mb-link").hide();
                        jQuery(".sticky-logo-link").show();
                        jQuery(".custom-logo-link").hide();
                    } 
                    else
                    {
                        jQuery('.header-sticky').removeClass('stickymenu');
                        jQuery(".sticky-logo-mb-link").hide();
                        jQuery(".sticky-logo-link").hide();
                        jQuery(".custom-logo-link").show();
                    }
                } 
            });
        });
    </script>
<?php  
}
elseif(get_theme_mod('ssh_device_type','desktop')=='desktop' && get_theme_mod('ssh_logo_enable',false)==false)
{ ?>
    <script type="text/javascript">
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (window.matchMedia("(max-width: 767px)").matches)  
                { 
                    //console.log("This is a mobile device."); 
                    jQuery('.spice.spice-custom').removeClass('stickymenu');
                } 
                else { 
                    // The viewport is at least 768 pixels wide 
                    if(jQuery(window).scrollTop() > 100) {
                        jQuery('.header-sticky').addClass('stickymenu');
                    } 
                    else {
                        jQuery('.header-sticky').removeClass('stickymenu');
                    }
                } 
            });
        });  
    </script>
 <?php 
}
elseif(get_theme_mod('ssh_device_type','desktop')=='mobile' && get_theme_mod('ssh_logo_enable',false)==false)
{ ?>
    <script>
    jQuery(document).ready(function ( jQuery ) {
        jQuery(window).bind('scroll', function () {
            if (window.matchMedia("(max-width: 767px)").matches)  
            { 
                //this script call only mobile device
                if (jQuery(window).scrollTop() > 100) {
                    jQuery('.header-sticky').addClass('stickymenu');
                } 
                else {
                    jQuery('.header-sticky').removeClass('stickymenu');
                }
            } 
            else { 
                // This script call only for Desktop Device 
                jQuery('.spice.spice-custom').removeClass('stickymenu');
            } 
        });
    });
    </script>
<?php
}
elseif(get_theme_mod('ssh_device_type','desktop')=='both' && get_theme_mod('ssh_logo_enable',false)==false)
{?>
    <script>
        jQuery(document).ready(function ( jQuery ) {
            jQuery(window).bind('scroll', function () {
                if (jQuery(window).scrollTop() > 100) {
                    jQuery('.header-sticky').addClass('stickymenu');
                } 
                else {
                    jQuery('.header-sticky').removeClass('stickymenu');
                }
            });
        });
    </script>
<?php } ?>