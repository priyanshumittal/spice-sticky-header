<?php
// Adding customizer home page setting
function ssh_customizer_controls($wp_customize)
{
 
    /* Theme Style setting */
    $wp_customize->add_panel('ssh_panel',
        array(
            'priority' => 139,
            'capability' => 'edit_theme_options',
            'title' => esc_html__('Sticky Header','spice-sticky-header')
        )
    );


    $wp_customize->add_section( 'ssh_customizer_section' , array(
        'title'      => esc_html__('General Settings', 'spice-sticky-header'),
        'priority'   => 1,
        'panel'  => 'ssh_panel',
    ) );

    $wp_customize->add_setting( 'sticky_header_enable', 
    array(
        'default'           => false,
        'sanitize_callback' => 'ssh_sanitize_checkbox'
    ));
    $wp_customize->add_control(new SSH_Toggle_Control( $wp_customize, 'sticky_header_enable',
        array(
            'label'     =>  esc_html__( 'Enable/Disable Sticky Header', 'spice-sticky-header'  ),
            'section'   =>  'ssh_customizer_section',
            'setting'   =>  'sticky_header_enable',
            'type'      =>  'toggle',
            'priority'          =>  1
        )
    ));

    // Enable/Disable different logo from sticky header
    $wp_customize->add_setting('ssh_logo_enable',
        array(
            'default'           => false,
            'sanitize_callback' => 'ssh_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new SSH_Toggle_Control( $wp_customize, 'ssh_logo_enable',
        array(
            'label'             =>  esc_html__( 'Different Logo for Sticky Header', 'spice-sticky-header'),
            'section'           =>  'ssh_customizer_section',
            'active_callback'   =>  'ssh_plugin_callback',
            'type'              =>  'toggle',
            'priority'          =>  2
        )
    ));


     // Stick Header logo for Desktop
    $wp_customize->add_setting( 'ssh_logo_enable_desktop', 
        array(
            'sanitize_callback' => 'esc_url_raw',
            ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ssh_logo_enable_desktop', 
        array(
            'label'             =>  esc_html__( 'Logo for desktop view', 'spice-sticky-header' ),
            'section'           =>  'ssh_customizer_section',
            'setting'          =>  'ssh_logo_enable_desktop',
            'active_callback'   =>  function($control) {
                                        return (
                                            ssh_plugin_callback($control) &&
                                            ssh_logo_callback($control)
                                        );
                                    },
            'priority'          =>  3
        ) 
    ));


    // Stick Header logo for Desktop
    $wp_customize->add_setting( 'ssh_logo_enable_mobile', 
        array(
            'sanitize_callback' => 'esc_url_raw',
            ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ssh_logo_enable_mobile', 
        array(
            'label'             =>  esc_html__( 'Logo for mobile view', 'spice-sticky-header' ),
            'section'           =>  'ssh_customizer_section',
            'setting'          =>  'ssh_logo_enable_mobile',
            'active_callback'   =>  function($control) {
                                        return (
                                            ssh_plugin_callback($control) &&
                                            ssh_logo_callback($control)
                                        );
                                    },
            'priority'          =>  4
        ) 
    ));


    // Enable sticky header on Desktop & Mobile view
    $wp_customize->add_setting('ssh_device_type',
        array(
            'default'           =>  'desktop',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'ssh_sanitize_select'
        )
    );
    $wp_customize->add_control('ssh_device_type', 
        array(
            'label'             =>  esc_html__('Enable','spice-sticky-header' ),
            'active_callback'   =>  'ssh_plugin_callback',
            'section'           =>  'ssh_customizer_section',
            'setting'           =>  'ssh_device_type',
            'type'              =>  'select',
            'priority'          =>  5,
            'choices'           =>  array(
                'desktop'       =>  esc_html__('Desktop', 'spice-sticky-header' ),
                'mobile'        =>  esc_html__('Mobile', 'spice-sticky-header' ),
                'both'          =>  esc_html__('Desktop + Mobile', 'spice-sticky-header')
            )
        )
    );


    // Add multiple options with widgets option
    $wp_customize->add_setting('ssh_animation_effect',
        array(
            'default'           =>  '',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'ssh_sanitize_select'
        )
    );
    $wp_customize->add_control('ssh_animation_effect', 
        array(
            'label'             => esc_html__('Animation Effect','spice-sticky-header' ),
            'active_callback'   =>  'ssh_plugin_callback',
            'section'           => 'ssh_customizer_section',
            'setting'           => 'ssh_animation_effect',
            'type'              => 'select',
            'priority'          =>  6,
            'choices'           =>  array(
                ''              =>  esc_html__('None', 'spice-sticky-header' ),
                'slide'         =>  esc_html__('Slide', 'spice-sticky-header' ),
                'fade'          =>  esc_html__('Fade', 'spice-sticky-header' ),
                'shrink'        =>  esc_html__('Shrink', 'spice-sticky-header')
            )
        )
    );


    // Sticky header opacity
    $wp_customize->add_setting('ssh_opacity',
        array(
            'default'       =>  1.0,
            'capability'    =>  'edit_theme_options'
        )
    );
    $wp_customize->add_control( new SSH_Slider_Custom_Control( $wp_customize, 'ssh_opacity',
        array(
            'label'             =>  esc_html__( 'Sticky Opacity', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_plugin_callback',
            'section'           =>  'ssh_customizer_section',
            'setting'           =>  'ssh_opacity',
            'priority'          =>  7,
            'input_attrs'   => array(
                'min'   => 0.1,
                'max'   => 1.0,
                'step'  => 0.1
            ),
        )
    ));
    
    // Sticky height
    $wp_customize->add_setting('ssh_height',
        array(
            'default'       =>  0,
            'capability'    =>  'edit_theme_options'
        )
    );
    $wp_customize->add_control( new SSH_Slider_Custom_Control( $wp_customize, 'ssh_height',
        array(
            'label'             =>  esc_html__( 'Sticky Height', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_plugin_callback',
            'section'           =>  'ssh_customizer_section',
            'setting'           =>  'ssh_height',
            'priority'          =>  8,
            'input_attrs'   => array(
                'min'   => 0,
                'max'   => 100,
                'step'  => 1
            ),
        )
    ));
    // Note for predefined images, background image, and background color
    class SSH_Sticky_Height_Note_Control extends WP_Customize_Control {

        public function render_content() {
            ?>
            <p><?php esc_html_e('Note: Sticky Height will not work with shrink effect', 'spice-sticky-header' ); ?></p>
            <?php
        }

    }
    $wp_customize->add_setting('sticky_height_note',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new SSH_Sticky_Height_Note_Control($wp_customize, 'sticky_height_note', 
        array(
            'section'           => 'ssh_customizer_section',
            'active_callback'   =>  'ssh_plugin_callback',
            'priority'          =>  9
        )
    ));



    /* ====================
    * Sticky Color Setting 
    ==================== */
    $wp_customize->add_section('ssh_clr_section', 
        array(
            'title'     => esc_html__('Color Settings', 'spice-sticky-header' ),
            'panel'  => 'ssh_panel',
            'priority'  => 2
        )
    );
    
    $wp_customize->add_setting('enable_sticky_header_clr',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'ssh_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new SSH_Toggle_Control( $wp_customize, 'enable_sticky_header_clr',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'spice-sticky-header'  ),
            'section'   =>  'ssh_clr_section',
            'setting'   =>  'enable_sticky_header_clr',
            'type'      =>  'toggle'
        )
    ));
    
    $wp_customize->add_setting('sticky_site_title_link_color', 
        array(
            'default'           => '#fff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_site_title_link_color', 
        array(
            'label'             =>  esc_html__('Site Title Color', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_color_callback',
            'section'           =>  'ssh_clr_section',
            'setting'           =>  'sticky_site_title_link_color'
        )
    ));
    
    $wp_customize->add_setting('sticky_site_tagline_link_color', 
        array(
            'default'           => '#c5c5c5',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_site_tagline_link_color', 
        array(
            'label'             =>  esc_html__('Site Tagline Color', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_color_callback',
            'section'           =>  'ssh_clr_section',
            'setting'           =>  'sticky_site_tagline_link_color'
        )
    ));
    
    $wp_customize->add_setting('sticky_menu_back_color', 
        array(
            'default'           => 'rgba(0,0,0,0.7)',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(new SSH_Alpha_Color_Control($wp_customize, 'sticky_menu_back_color', 
        array(
            'label'             =>  esc_html__('Background Color', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_color_callback',
            'section'           =>  'ssh_clr_section',
            'setting'           =>  'sticky_menu_back_color'
        )
    ));
    
    class SSHMenu_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Menu', 'spice-sticky-header' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('sticky_header_menu',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new SSHMenu_Customize_Control($wp_customize, 'sticky_header_menu', 
        array(
            'section'           =>  'ssh_clr_section',
            'active_callback'   =>  'ssh_color_callback',
            'setting'           =>  'sticky_header_menu',
        )
    ));
    
    $wp_customize->add_setting('sticky_menu_link_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_menu_link_color', 
        array(
            'label'             =>  esc_html__('Text/Link Color', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_color_callback',
            'section'           =>  'ssh_clr_section',
            'setting'           =>  'sticky_menu_link_color'
        )
    ));
  
    $wp_customize->add_setting('sticky_menu_link_hover_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_menu_link_hover_color', 
        array(
            'label'             =>  esc_html__('Link Hover Color', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_color_callback',
            'section'           =>  'ssh_clr_section',
            'setting'           =>  'sticky_menu_link_hover_color'
        )
    ));
    
    $wp_customize->add_setting('sticky_menu_active_link_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_menu_active_link_color', 
        array(
            'label'             =>  esc_html__('Active Link Color', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_color_callback',
            'section'           =>  'ssh_clr_section',
            'setting'           =>  'sticky_menu_active_link_color'
        )
    ));
    
    class SSHSubmenu_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Submenus', 'spice-sticky-header' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('sticky_header_submenu',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(new SSHSubmenu_Customize_Control($wp_customize, 'sticky_header_submenu', 
        array(
            'section'           =>  'ssh_clr_section',
            'active_callback'   =>  'ssh_color_callback',
            'setting'           =>  'sticky_header_submenu',
        )
    ));
    
    $wp_customize->add_setting('sticky_submenu_back_color', 
        array(
            'default'           => '#21202e',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_submenu_back_color', 
        array(
            'label'             =>  esc_html__('Background Color', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_color_callback',
            'section'           =>  'ssh_clr_section',
            'setting'           =>  'sticky_submenu_back_color'
        )
    ));
    
    $wp_customize->add_setting('sticky_submenu_link_color', 
        array(
            'default'           => '#d5d5d5',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_submenu_link_color', 
        array(
            'label'             =>  esc_html__('Text/Link Color', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_color_callback',
            'section'           =>  'ssh_clr_section',
            'setting'           =>  'sticky_submenu_link_color'
        )
    ));
   
    $wp_customize->add_setting('sticky_submenu_link_hover_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_submenu_link_hover_color', 
        array(
            'label'             =>  esc_html__('Link Hover Color', 'spice-sticky-header' ),
            'active_callback'   =>  'ssh_color_callback',
            'section'           =>  'ssh_clr_section',
            'setting'           =>  'sticky_submenu_link_hover_color'
        )
    ));


}

add_action( 'customize_register', 'ssh_customizer_controls' );