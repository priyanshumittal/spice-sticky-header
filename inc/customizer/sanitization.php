<?php
/**
 * Sanitization Callbacks
 * 
 * @package Spice Sticky Header Plugin
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


// callback function for the sticky header
function ssh_plugin_callback($control) {
    if (false == $control->manager->get_setting('sticky_header_enable')->value()) {
        return false;
    } else {
        return true;
    }
}


// callback function for the sticky header different logo
function ssh_logo_callback($control) {
    if (false == $control->manager->get_setting('ssh_logo_enable')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the header background color
function ssh_color_callback($control) {
    if (false == $control->manager->get_setting('enable_sticky_header_clr')->value()) {
        return false;
    } else {
        return true;
    }
}



/**
 * Select choices sanitization callback
*/
function ssh_sanitize_select($input, $setting) {

    //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
    $input = sanitize_key($input);

    //get the list of possible radio box options 
    $choices = $setting->manager->get_control($setting->id)->choices;

    //return input if valid or return default option
    return ( array_key_exists($input, $choices) ? $input : $setting->default );
}

/**
 * Checkbox sanitization callback
*/
function ssh_sanitize_checkbox($checked) {

    // Boolean check.
    return ( ( isset($checked) && true == $checked ) ? true : false );

}