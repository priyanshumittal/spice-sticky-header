<?php 
/**
* Sticy Header Controls
*
* @package Spice Sticky Header Plugin
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Spice_Sticky_Header_Customizer' ) ) :

	/**
	 * Spice Sticky Header Customizer class
	*/
	class Spice_Sticky_Header_Customizer {

		/**
		 * Setup class
		*/
		public function __construct() 
		{
			add_action( 'customize_register', array( $this, 'ssh_custom_controls' ) );
			add_action( 'after_setup_theme', array( $this, 'ssh_register_options' ) );
			add_action( 'wp_enqueue_scripts', array( $this,'ssh_load_script'));
		}


		/**
		 * Adds custom controls
		*/
		public function ssh_custom_controls( $wp_customize ) 
		{
			require_once ( SSH_PLUGIN_DIR . '/inc/customizer/controls/toggle/class-toggle-control.php' );
			require_once ( SSH_PLUGIN_DIR . '/inc/customizer/controls/range/range-control.php' );
			require_once ( SSH_PLUGIN_DIR . '/inc/customizer/controls/color/color-control.php' );
			$wp_customize->register_control_type('SSH_Toggle_Control');
		}

		/**
		 * Adds customizer options
		*/
		public function ssh_register_options() 
		{
			require_once ( SSH_PLUGIN_DIR . '/inc/customizer/sanitization.php' );
			require_once ( SSH_PLUGIN_DIR . '/inc/customizer/customizer.php' );
		}

		/**
		 * Load Js
		*/
		public function ssh_load_script()
	     {
	      if(get_theme_mod('preloader_enable',false)== true):
	      	wp_enqueue_script('ssh-sticky', SSH_PLUGIN_URL . 'js/custom.js', array('jquery'));
	  	  endif;
	      wp_enqueue_style('ssh-custom',  SSH_PLUGIN_URL. 'css/custom.css');
	     }

	}

endif;

new Spice_Sticky_Header_Customizer();	