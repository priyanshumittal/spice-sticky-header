(function ($) {   
     /* Preloader */
    jQuery(window).on('load', function() {
        var preloaderFadeOutTime = 500;
        function olivewp_hidePreloader() {
            var preloader = jQuery('.olivewp-loader');
            setTimeout(function() {
                preloader.fadeOut(preloaderFadeOutTime);
            }, 500);
        }
        olivewp_hidePreloader();
    });
})(jQuery);