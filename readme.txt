=== Spice Sticky Header ===

Contributors: 		spicethemes
Tags: 				sticky header
Requires at least: 	5.3
Requires PHP: 		5.2
Tested up to: 		6.2
Stable tag: 		1.0
License: 			GPLv2 or later
License URI: 		https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Enhances functionality of header.

== Changelog ==

@Version 1.0
* Updated freemius directory.

@Version 0.2
* Updated freemius code.

@Version 0.1
* Initial Release.