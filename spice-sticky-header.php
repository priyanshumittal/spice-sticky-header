<?php 
/*
* Plugin Name:          Spice Sticky Header
* Plugin URI:  			
* Description: 		    Enhances functionality of header.
* Version:              1.0
* Requires at least:    5.3
* Requires PHP: 	    5.2
* Tested up to: 	    6.2
* Author:               spicethemes
* Author URI:  		    https://spicethemes.com
* License: 			    GPLv2 or later
* License URI: 		    https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: 		    spice-sticky-header
* Domain Path:  	    /languages
*/



if ( ! function_exists( 'ssh_fs' ) ) {
    // Create a helper function for easy SDK access.
    function ssh_fs() {
        global $ssh_fs;

        if ( ! isset( $ssh_fs ) ) {
            // Include Freemius SDK.
            if ( function_exists('olivewp_companion_activate') && defined( 'OWC_PLUGIN_DIR' ) && file_exists(OWC_PLUGIN_DIR . '/inc/freemius/start.php') ) {
                // Try to load SDK from olivewp companion folder.
                require_once OWC_PLUGIN_DIR . '/inc/freemius/start.php';
            } else if ( function_exists('olivewp_plus_activate') && defined( 'OLIVEWP_PLUGIN_DIR' ) && file_exists(OLIVEWP_PLUGIN_DIR . '/freemius/start.php') ) {
                // Try to load SDK from premium olivewp companion plugin folder.
                require_once OLIVEWP_PLUGIN_DIR . '/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }

            $ssh_fs = fs_dynamic_init( array(
                'id'                  => '10576',
                'slug'                => 'spice-sticky-header',
                'premium_slug'        => 'spice-sticky-header',
                'type'                => 'plugin',
                'public_key'          => 'pk_4c4751fb21516e4900bdcdd17d01a',
                'is_premium'          => true,
                'is_premium_only'     => true,
                'has_paid_plans'      => true,
                'is_org_compliant'    => false,
                'menu'                => array(
                    'support'        => false,
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
               ) );
        }

        return $ssh_fs;
    }
ssh_fs();
}

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// define the constant for the URL
define( 'SSH_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SSH_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );


add_action( 'init', 'ssh_load_textdomain' );
/**
 * Load plugin textdomain.
 */
function ssh_load_textdomain() {
  load_plugin_textdomain( 'spice-sticky-header', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

//Define the function for the plugin activation
function ssh_activation() {
 
	require_once SSH_PLUGIN_DIR . 'inc/customizer/customizer-class.php';

	if(! function_exists ( 'olivewp_plus_activate' ) ):
	require_once SSH_PLUGIN_DIR . 'inc/header-preset/header.php';
	endif;

	require_once SSH_PLUGIN_DIR . 'inc/sticky.php';

}
add_action('plugins_loaded', 'ssh_activation');